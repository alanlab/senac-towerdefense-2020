%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: PlayerHitLayerMask
  m_Mask: 01000000010000000100000001000000010000000000000000000000000000000000000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: HumanArmature
    m_Weight: 1
  - m_Path: HumanArmature/Bone
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/Hips
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Neck
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Neck/Head
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L/UpperArm.L
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L/UpperArm.L/LowerArm.L
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L/UpperArm.L/LowerArm.L/Palm.L
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L/UpperArm.L/LowerArm.L/Palm.L/MiddleHand.L
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L/UpperArm.L/LowerArm.L/Palm.L/MiddleHand.L/Fingers.L
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L/UpperArm.L/LowerArm.L/Palm.L/MiddleHand.L/Fingers.L/Fingers.L_end
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L/UpperArm.L/LowerArm.L/Thumb.R
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L/UpperArm.L/LowerArm.L/Thumb.R/Thumb2.R
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.L/UpperArm.L/LowerArm.L/Thumb.R/Thumb2.R/Thumb2.R_end
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R/UpperArm.R
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R/UpperArm.R/LowerArm.R
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R/UpperArm.R/LowerArm.R/Palm.R
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R/UpperArm.R/LowerArm.R/Palm.R/MiddleHand.R
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R/UpperArm.R/LowerArm.R/Palm.R/MiddleHand.R/Fingers.R
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R/UpperArm.R/LowerArm.R/Palm.R/MiddleHand.R/Fingers.R/Fingers.R_end
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R/UpperArm.R/LowerArm.R/Thumb.L
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R/UpperArm.R/LowerArm.R/Thumb.L/Thumb2.L
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/Hips/Abdomen/Torso/Shoulder.R/UpperArm.R/LowerArm.R/Thumb.L/Thumb2.L/Thumb2.L_end
    m_Weight: 0
  - m_Path: HumanArmature/Bone/Body/UpperLeg.L
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/UpperLeg.L/LowerLeg.L
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/UpperLeg.L/LowerLeg.L/LowerLeg.L_end
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/UpperLeg.R
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/UpperLeg.R/LowerLeg.R
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Body/UpperLeg.R/LowerLeg.R/LowerLeg.R_end
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Foot.L
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Foot.L/Foot.L_end
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Foot.R
    m_Weight: 1
  - m_Path: HumanArmature/Bone/Foot.R/Foot.R_end
    m_Weight: 1
  - m_Path: HumanArmature/Bone/PoleTarget.L
    m_Weight: 0
  - m_Path: HumanArmature/Bone/PoleTarget.L/PoleTarget.L_end
    m_Weight: 0
  - m_Path: HumanArmature/Bone/PoleTarget.R
    m_Weight: 0
  - m_Path: HumanArmature/Bone/PoleTarget.R/PoleTarget.R_end
    m_Weight: 0
  - m_Path: Knight
    m_Weight: 0
