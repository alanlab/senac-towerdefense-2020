﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEvents : MonoBehaviour {

    public Collider swordCollider = null;

    public void ActivateCollider()
    {
        swordCollider.enabled = true;
    }

    public void DeactivateCollider()
    {
        swordCollider.enabled = false;
    }

}

