﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public int vidaAtual = 10;
    public int vidaMax = 10;
    public int ataque = 3;
    public bool dead, stop = false;

    public float invulnerableTime = .3f;

    [Header("HUD Settings")]
    public Slider healthSlider = null;

    //Uso interno
    private float damageCooldown = 0f;

    //Reference to Controller
    private PlayerController pc = null;

    private void Start()
    {
        pc = GetComponent<PlayerController>();
        HUDAtualizaVida();
    }

    public void Para()
    {
        stop = true;
        pc.playerAnimator.SetBool("running", false);
    }

    public void RecebeDano(int dano)
    {
        if (Time.time > damageCooldown)
        {
            damageCooldown = Time.time + invulnerableTime;

            pc.playerAnimator.SetTrigger("hit");
            vidaAtual -= dano;

            HUDAtualizaVida();

            if (vidaAtual <= 0)
            {
                Morreu();
            }
        }
    }

    private void HUDAtualizaVida()
    {
        if (healthSlider != null)
        {
            healthSlider.value = (float)vidaAtual / (float)vidaMax;
        }
    }

    private void Morreu()
    {
        Debug.Log("Game Over");
        dead = true;
        pc.playerAnimator.SetBool("dead", true);
        GameManager.instance.ChamaGameOver();
    }

}

