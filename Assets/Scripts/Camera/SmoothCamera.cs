﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCamera : MonoBehaviour {

    //Alvo da Câmera
    public Transform target = null;

    public float smoothTime = 0.15F;
    private Vector3 velocity = Vector3.zero;

    //Distância entre o Player e a Câmera
    public Vector3 offset;

    [Header("Limites")]
    public bool enableBoundary = false;
    public float maxHorizontal = 100f;
    public float minHorizontal = -100f;
    public float maxVertical = 100f;
    public float minVertical = -100f;

    private void Update ()
    {
        Vector3 desiredPosition = target.position + offset;

        if(enableBoundary)
        {
            desiredPosition.x = Mathf.Clamp(desiredPosition.x, minHorizontal, maxHorizontal);
            desiredPosition.z = Mathf.Clamp(desiredPosition.z, minVertical, maxVertical);
        }

        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothTime);
    }

}
