﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TowerProjectile : MonoBehaviour {

    private int ataque = 1;

    public void SegueAlvo(int _ataque, Enemy _enemy, Transform targetPosition, float timeToHit)
    {
        //Recebe o valor do ataque da Torre
        ataque = _ataque;

        //Realiza um Tween até a posição de Hit do Inimigo
        //Configura uma suavização no começo (In) e no final (Out), deixando o meio mais rápido.
        //Quando o tween termina, ele fará um "Callback", uma chamada, a função acertou.
        transform.DOMove(targetPosition.position, timeToHit, true)
            .SetEase(Ease.InOutQuad)    
            .OnComplete(() => Acertou(_enemy)); 
    }

    private void Acertou(Enemy _enemy)
    {
        if(_enemy != null && !_enemy.dead)
        {
            _enemy.RecebeDano(ataque);
        }
        //Você pode também usar Destroy(gameObject, .5f);
        StartCoroutine(EsperaDestroi());
    }

    //Corotina para esperar o rastro terminar
    //e evitar sumir as partículas abruptamente;
    private IEnumerator EsperaDestroi()
    {
        
        yield return new WaitForSeconds(.5f); 
        Destroy(gameObject);
    }

    
}
