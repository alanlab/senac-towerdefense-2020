﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpawnPosition
{
    top,
    left,
    right,
    bottom
}

public class EnemiesManager : MonoBehaviour
{
    [Header("Wave Settings")]
    public float waitTime = 3f;
    public List<Wave> waveList = new List<Wave>();
    public List<Transform> topSpawnPoints, leftSpawnPoints, rightSpawnPoints, bottomSpawnPoints;
    public int waveTotalNumber = 1;

    [Header("Other Settings")]
    public bool stop = false;
    public int currentEnemiesNumber = 0;
    public int maxEnemies = 100;

    //Uso interno
    private int currentWaveIndex = 0;
    private int waveCount = 0;
    private List<Enemy> enemiesInScene = new List<Enemy>();

    //Configura os Spawn Points que serão utilizados
    private List<Vector3> spawns = new List<Vector3>();
    private int sizeSpawns = 0;
    private Wave wave = null;

    private void Start()
    {
        waveCount = waveList.Count - 1;
    }

    private void Update()
    {
        if(Time.time > waitTime && !stop)
        {
            ConfiguraProximaWave();
            GameManager.instance.ui.AtualizaWaveUI();
            waveTotalNumber++;          
        }
    }

    private void ConfiguraProximaWave()
    {
        //Tempo de Espera para a próxima Wave
        waitTime = Time.time + waveList[currentWaveIndex].waveTime;

        ConfiguraSpawnPoints();

        //Aumenta o index para pegar a próxima wave, 
        //caso a wave atual não seja o último da lista de Waves
        if (currentWaveIndex < waveCount)
        {
            currentWaveIndex++;
        }
    }

    private void ConfiguraSpawnPoints()
    {
        //Configura os Spawn Points que serão utilizados
        spawns.Clear();
        wave = waveList[currentWaveIndex];

        if(wave.randomSpawnPoint)
        {
            wave.spawnPoint = (SpawnPosition)Random.Range(0, 4);
        }

        switch (wave.spawnPoint)
        {
            case SpawnPosition.top:
                sizeSpawns = topSpawnPoints.Count;
                for (int i = 0; i < sizeSpawns; i++)
                {
                    spawns.Add(topSpawnPoints[i].position);
                }
                break;
            case SpawnPosition.left:
                sizeSpawns = leftSpawnPoints.Count;
                for (int i = 0; i < sizeSpawns; i++)
                {
                    spawns.Add(leftSpawnPoints[i].position);
                }
                break;
            case SpawnPosition.right:
                sizeSpawns = rightSpawnPoints.Count;
                for (int i = 0; i < sizeSpawns; i++)
                {
                    spawns.Add(rightSpawnPoints[i].position);
                }
                break;
            case SpawnPosition.bottom:
                sizeSpawns = bottomSpawnPoints.Count;
                for (int i = 0; i < sizeSpawns; i++)
                {
                    spawns.Add(bottomSpawnPoints[i].position);
                }
                break;
        }

        ChamaProximaWave();
            
    }

    public void ChamaProximaWave()
    {
        int sizePrefabs = wave.waveItems.Count;

        //Estrutura de Repetição que irá gerar os inimigos
        for (int i = 0; i < sizePrefabs; i++)
        {
            for (int j = 0; j < wave.waveItems[i].enemyAmount; j++)
            {
                if (currentEnemiesNumber < maxEnemies)
                {
                    int randomSpawn = Random.Range(0, sizeSpawns);

                    Enemy newEnemy = Instantiate(wave.waveItems[i].enemyPrefab,
                                                    spawns[randomSpawn],
                                                    Quaternion.identity,
                                                    transform)
                                                    .GetComponent<Enemy>();

                    newEnemy.ataque = (int)(newEnemy.ataque * wave.attackMultiplier);
                    newEnemy.vidaAtual = newEnemy.vidaMax = (int)(newEnemy.vidaAtual * wave.healthMultiplier);
                    newEnemy.scorePoints = (int)(newEnemy.scorePoints * wave.scoreMultiplier);
                    enemiesInScene.Add(newEnemy);
                    currentEnemiesNumber++;
                }
                else
                {
                    break;
                }
            }
        }
    }

    public void RemoveInimigo(Enemy e)
    {
        if(!stop)
        {
            enemiesInScene.Remove(e);
            currentEnemiesNumber--;
        }
    }

    public void Para()
    {
        stop = true;
        foreach(Enemy e in enemiesInScene)
        {
            if(e != null)
            {
                e.Para();
            }
        }
    }

}//END of Class


