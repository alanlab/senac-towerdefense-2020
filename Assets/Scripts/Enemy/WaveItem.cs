﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "waveItem", menuName = "Create Wave Item Data", order = 1)]
public class WaveItem : ScriptableObject {

    public GameObject enemyPrefab;
    public int enemyAmount;

}
