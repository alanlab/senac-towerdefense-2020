﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "wave", menuName = "Create Wave Data", order = 0)]
public class Wave : ScriptableObject
{

    [Header("Time Settings")]
    [Tooltip("After the wave starts, here is the time of how long the system will wait to go to the next wave.\nIn Seconds.")]
    public float waveTime = 60;

    [Space(10)]

    [Header("Wave Settings")]
    public bool randomSpawnPoint = false;
    public SpawnPosition spawnPoint;
    [Tooltip("Set here the Wave items that will compose this Wave.\nEach Wave Item contains a Enemy Prefab and the amount of them to be instantiated in the Wave.")]
    public List<WaveItem> waveItems;

    [Space(10)]

    [Header("Stats Settings")]
    [Tooltip("Multiply the health of all enemies in this wave by the given value")]
    [Range(0.25f, 4f)]
    public float healthMultiplier = 1f;
    [Tooltip("Multiply the attack of all enemies in this wave by the given value")]
    [Range(0.25f, 4f)]
    public float attackMultiplier = 1f;
    [Tooltip("Multiply the score received of all enemies in this wave by the given value")]
    [Range(1f, 10f)]
    public float scoreMultiplier = 1f;

}
