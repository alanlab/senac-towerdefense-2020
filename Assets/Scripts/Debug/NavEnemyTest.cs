﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavEnemyTest : MonoBehaviour {

    public NavMeshAgent agent;
    public Transform position;

    public void GoTo()
    {
        agent.SetDestination(position.position);
    }

}
