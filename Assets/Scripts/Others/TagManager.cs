﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TagItem
{
    player,
    enemy,
    tower
}

public class TagManager {

    public static string EnemyTag = "Enemy";
    public static string PlayerTag = "Player";
    public static string TowerTag = "Tower";

    public static string TagName(TagItem item)
    {
        switch(item)
        {
            case TagItem.player:
                return PlayerTag;
            case TagItem.enemy:
                return EnemyTag;
            case TagItem.tower:
                return TowerTag;
            default:
                return PlayerTag;
        }
    }

}
