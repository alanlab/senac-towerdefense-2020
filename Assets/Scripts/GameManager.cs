﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour {

    [HideInInspector]
    public static GameManager instance;

    [Header("Reference Settings")]
    public UIManager ui;
    public EnemiesManager em;

    [Header("Targeting Settings")]
    public Player player;
    public Core core;
    public Tower[] towers = new Tower[4];

    [Header("Game Settings")]
    public bool gameOver = false;
    public int score = 0;

    //Configura o acesso estático ao UIManager
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public Tower RecebeTorreMaisProxima(Vector3 posicao)
    {
        Tower t = null;
        float distancia = float.MaxValue;

        for (int i = 0; i < 4; i++)
        {
            if (!towers[i].dead)
            {
                float temp = Mathf.Abs((towers[i].transform.position - posicao).sqrMagnitude);
                if (temp < distancia)
                {
                    distancia = temp;
                    t = towers[i];
                }
            }
        }

        return t;
    }

    public void AtualizaScore(int valor)
    {
        score += valor;
        ui.AtualizaScoreUI();
    }

    public void ParaTorres()
    {
        core.Para();
        for (int i = 0; i < 4; i++)
        {
            towers[i].Para();
        }
    }

    public void ChamaGameOver()
    {
        gameOver = true;
        player.Para();
        ParaTorres();
        em.Para();
        ui.ChamaTelaGameOver();
    }

}
