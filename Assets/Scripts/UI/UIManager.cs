﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public GameObject panelGameOver = null;

    public Text waveText = null;
    public Text scoreText = null;

    public void ChamaTelaGameOver()
    {
        if(panelGameOver != null)
        {
            panelGameOver.SetActive(true);
        }
    }

    public void AtualizaScoreUI()
    {
        if(scoreText != null)
        {
            scoreText.text = GameManager.instance.score.ToString();
        }
    }

    public void AtualizaWaveUI()
    {
        if (waveText != null)
        {
            waveText.text = "Wave " + GameManager.instance.em.waveTotalNumber.ToString();
        }
    }

}
